import os

# name to show in rabbitmq queue list
extractorName = os.getenv('RABBITMQ_QUEUE', "4ceed.dm3.metadata.preview")

# URL to be used for connecting to rabbitmq
rabbitmqURL = os.getenv('RABBITMQ_URI', "amqp://guest:guest@localhost/%2f")

# name of rabbitmq exchange
rabbitmqExchange = os.getenv('RABBITMQ_EXCHANGE', "clowder")

# type of files to process
messageType = "*.file.digitalmicrograph.#"

# trust certificates, set this to false for self signed certificates
sslVerify = os.getenv('RABBITMQ_SSLVERIFY', False)

# image generating binary, or None if none is to be generated
imageBinary = "convert"

# image preview type
imageType = "png"

# image thumbnail command line
imageThumbnail = "@BINARY@ @INPUT@ -resize 225^ @OUTPUT@"

# image preview command line
imagePreview = "@BINARY@ @INPUT@ -resize 800x600 @OUTPUT@"

# type specific preview, or None if none is to be generated
previewBinary = None

# type preview type
previewType = None

# type preview command line
previewCommand = None

# trust certificates, set this to false for self signed certificates
sslVerify = os.getenv('RABBITMQ_SSLVERIFY', False)

# image generating binary, or None if none is to be generated
imageBinary = "convert"

# image preview type
imageType = "png"

# image thumbnail command line
imageThumbnail = "@BINARY@ @INPUT@ -resize 225^ @OUTPUT@"

# image preview command line
imagePreview = "@BINARY@ @INPUT@ -resize 800x600 @OUTPUT@"

# type specific preview, or None if none is to be generated
previewBinary = None

# type preview type
previewType = None

# type preview command line
previewCommand = None