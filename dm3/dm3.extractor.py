#!/usr/bin/env python

import logging
import os
import re
import subprocess
import tempfile
import dm3reader_v072

import shutil
from pyclowder.extractors import Extractor
import pyclowder.files
import pyclowder.utils


class DigitalMicrographDm3Extractor(Extractor):
    """Count the number of characters, words and lines in a text file."""

    def __init__(self):
        Extractor.__init__(self)

        image_binary = os.getenv('IMAGE_BINARY', '/usr/bin/convert')

        # add any additional arguments to parser
        self.parser.add_argument('--image-binary', nargs='?', dest='image_binary', default=image_binary,
                                 help='Image Binary used to for image thumbnail/preview (default=%s)' % image_binary)

        # parse command line and load default logging configuration
        self.setup()

        # setup logging for the exctractor
        logging.getLogger('pyclowder').setLevel(logging.DEBUG)
        logging.getLogger('__main__').setLevel(logging.DEBUG)

    def process_message(self, connector, host, secret_key, resource, parameters):
        # Process the file and upload the results

        inputfile = resource["local_paths"][0]
        file_id = resource['id']

        tags = dm3reader_v072.extract_dm3_metadata(inputfile, dump=False)
        top_tags = dm3reader_v072.get_metadata_shortlist(**tags)
        all_metadata = dict()
        all_metadata["top_metadata"] = top_tags
        all_metadata["all_metadata"] = tags

        metadata = self.get_metadata(all_metadata, 'file', resource['id'], host)

        pyclowder.files.upload_metadata(connector, host, secret_key, file_id, metadata)

        image_preview_png = dm3reader_v072.make_pgm_tempfile(inputfile)

        (fd, tmpfile) = tempfile.mkstemp(suffix='.' + 'png')
        (fd1, tmpfile1) = tempfile.mkstemp(suffix='.' + 'png')
        try:
            os.close(fd)
            commands = self.args.image_binary + ' ' + image_preview_png + ' ' + '-resize 800x600' + ' ' + tmpfile
            commands_thumbnail = self.args.image_binary + ' ' + image_preview_png + ' ' + '-resize 225^' + ' ' + tmpfile1
            p = re.compile(r'''((?:[^ "']|"[^"]*"|'[^']*')+)''')
            commandline = p.split(commands)[1::2]
            commandline_thumbnail = p.split(commands_thumbnail)[1::2]
            image_preview_to_upload = subprocess.check_output(commandline, stderr=subprocess.STDOUT)
            image_thumbnail_to_upload = subprocess.check_output(commandline_thumbnail, stderr=subprocess.STDOUT)
        except:
            logging.error("Could not generate preview")

        pyclowder.files.upload_preview(connector, host, secret_key, file_id, tmpfile, None)
        pyclowder.files.upload_thumbnail(connector, host, secret_key, file_id, tmpfile1)

        try:
            os.remove(tmpfile)
            os.remove(tmpfile1)
        except:
            logging.error("could not remove temporary files")
            logging.error(e)


if __name__ == "__main__":
    extractor = DigitalMicrographDm3Extractor()
    extractor.start()
