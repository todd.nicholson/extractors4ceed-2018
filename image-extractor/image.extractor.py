#!/usr/bin/env python

import logging
import os
import re
import subprocess
import tempfile

import shutil
from pyclowder.extractors import Extractor
import pyclowder.files
import pyclowder.utils


class ImageMetadataExtractor(Extractor):
    """Count the number of characters, words and lines in a text file."""
    def __init__(self):
        Extractor.__init__(self)

        image_binary = os.getenv('IMAGE_BINARY', '/usr/bin/convert')
        # image_binary = 'convert'

        # add any additional arguments to parser
        self.parser.add_argument('--image-binary', nargs='?', dest='image_binary', default=image_binary,
                                 help='Image Binary used to for image thumbnail/preview (default=%s)' % image_binary)

        # parse command line and load default logging configuration
        self.setup()

        # setup logging for the exctractor
        logging.getLogger('pyclowder').setLevel(logging.DEBUG)
        logging.getLogger('__main__').setLevel(logging.DEBUG)

    def process_message(self, connector, host, secret_key, resource, parameters):
        # Process the file and upload the results

        inputfile = resource["local_paths"][0]
        file_id = resource['id']

        (fd, tmpfile) = tempfile.mkstemp(suffix='.' + 'png')
        (fd1, tmpfile1) = tempfile.mkstemp(suffix='.' + 'png')

        try:
            os.close(fd)
            logging.info("starting extraction of image")
            commandLine= [self.args.image_binary, inputfile, ' -resize 800x600 ', tmpfile]
            commands = self.args.image_binary + ' ' + inputfile + ' ' + '-resize 800x600' + ' ' + tmpfile
            commands_thumbnail = self.args.image_binary + ' ' + inputfile + ' ' + '-resize 225^' + ' ' + tmpfile1

            p = re.compile(r'''((?:[^ "']|"[^"]*"|'[^']*')+)''')
            commandline = p.split(commands)[1::2]
            commandline_thumbnail = p.split(commands_thumbnail)[1::2]
            try:
                image_preview = subprocess.check_output(commandline, stderr=subprocess.STDOUT)
            except:
                logging.error("failed to generate preview")
            try:
                logging.info("Attempting to post image preview here")
                pyclowder.files.upload_preview(connector, host, secret_key, file_id, tmpfile, None)
            except Exception as e:
                logging.error(e)
                logging.error("Could not upload preview")

            image_thumbnail = subprocess.check_output(commandline_thumbnail, stderr=subprocess.STDOUT)
            try:
                pyclowder.files.upload_thumbnail(connector, host, secret_key, file_id, tmpfile1)
            except Exception as e:
                print("could not post thumbnail")
                print(e)

        except:
            logging.error("Could not create image preview")

if __name__ == "__main__":
    extractor = ImageMetadataExtractor()
    extractor.start()