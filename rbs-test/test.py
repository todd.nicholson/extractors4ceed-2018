import sims_xrna
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt

test_spe = 'Au_Cu_Si_01.spe'

test_xnra = 'Pt_Mn_Si_Scan01.xnra'

#lines = sims_spe.getLines(test_spe)

asDict = sims_xrna.getAsDict(test_xnra)

def get_cmap(n, name='hsv'):
    '''Returns a function that maps each index in 0, 1, ..., n-1 to a distinct
    RGB color; the keyword argument name must be a standard mpl colormap name.'''
    return plt.cm.get_cmap(name, n)

cmap = get_cmap(30)

first = cmap(0)

cmap2 = sims_xrna.generateColorList(30)

sims_xrna.plotDataAndSimulations(asDict,'/Users/helium/Desktop/test2.png')
plt.clf()

sims_xrna.plotSimulations(asDict,'/Users/helium/Desktop/simulations2.png')


# ISOTOPES appear to work

sims_xrna.plotIsotope(asDict, '/Users/helium/Desktop/isotopes2.png')
plt.clf()

#TODO does not work
sims_xrna.plotElemental(asDict, '/Users/helium/Desktop/elemental2.png')


#TODO does not work
sims_xrna.plotDataAndSmoothed(asDict, '/Users/helium/Desktop/dsmoothe2.png')