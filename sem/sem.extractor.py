#!/usr/bin/env python

import datetime
import logging
import tempfile
import math
import os
import subprocess
import json

from pyclowder.extractors import Extractor
from pyclowder.utils import CheckMessage
from pyclowder.utils import StatusMessage
import pyclowder.files
import pyclowder.datasets


class SemExtractor(Extractor):

    def __init__(self):
        Extractor.__init__(self)

        # add any additional arguments to parser

        # parse command line and load default logging configuration
        self.setup()

        # setup logging for the exctractor
        logging.getLogger('pyclowder').setLevel(logging.DEBUG)
        logging.getLogger('__main__').setLevel(logging.DEBUG)

        # assign other arguments

    def check_message(self, connector, host, secret_key, resource, parameters):
        files_of_dataset = resource['files']
        triggering_file = resource['triggering_file']
        last_file = files_of_dataset[-1]
        triggering_file_extension = str(os.path.splitext(triggering_file)[1])
        if triggering_file_extension == '.txt' or triggering_file_extension == '.tif' or triggering_file_extension == '.tiff':
            return True
        else:
            return False


    def process_message(self, connector, host, secret_key, resource, parameters):
        starttime = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
        logging.debug("Started computing images at %s" % str(starttime))
        files_of_dataset = resource['files']
        triggering_file = resource['triggering_file']
        last_file = files_of_dataset[-1]
        last_file_name = last_file['filename']
        last_file_extension = str(os.path.splitext(last_file_name)[1])
        if last_file_extension == '.txt':
            matching_tif_files = self.find_matching_file(last_file_name, files_of_dataset)
            if len(matching_tif_files) > 0:
                try:
                    logging.info('the last file', last_file)
                    last_file_content = pyclowder.files.download(connector, host, secret_key, last_file['id'], intermediatefileid=None, ext=".txt")
                    file_md = self.get_metadata_from_text(last_file_content)

                    for tif_file in matching_tif_files:
                        current_md = pyclowder.files.download_metadata(connector, host, secret_key, tif_file['id'])
                        if len(current_md) == 0:
                            metadata = self.get_metadata(file_md, 'file', tif_file['id'], host)
                            pyclowder.files.upload_metadata(connector, host, secret_key, tif_file['id'], metadata)
                        elif str(current_md[0]['agent']['name']).find('sem') != -1:
                            logging.info('sem extractor already ran')
                        else:
                            metadata = self.get_metadata(file_md, 'file', tif_file['id'], host)
                            pyclowder.files.upload_metadata(connector, host, secret_key, tif_file['id'], metadata)
                except Exception as e:
                    logging.error(e)
                    logging.info("Error in SEM extractor")

        else:
            matching_text_files = self.find_matching_file(last_file_name, files_of_dataset)
            if len(matching_text_files) > 0:
                matching_file = matching_text_files[0]
                matching_text_file_content = pyclowder.files.download(connector, host, secret_key, matching_file['id'],
                                                             intermediatefileid=None, ext=".txt")
                try:
                    file_md = self.get_metadata_from_text(matching_text_file_content)
                    metadata = self.get_metadata(file_md, 'file', last_file['id'], host)
                    pyclowder.files.upload_metadata(connector, host, secret_key, last_file['id'], metadata)
                except Exception as e:
                    logging.error(e)
                    logging.error("Error adding metadata to SEM tif image")

    def find_matching_file(self, filename, file_list):
        base_name = os.path.splitext(filename)[0]
        filename_extension = os.path.splitext(filename)[1]
        matches = []
        for current_file in file_list:
            current_file_name = current_file['filename']
            current_file_base_name = os.path.splitext(current_file_name)[0]
            current_file_extension = str(os.path.splitext(current_file_name)[1])
            if (current_file_base_name == base_name) and (current_file_extension != filename_extension):
                if current_file_extension == '.txt' or current_file_extension.lower() == '.tif' or '.tiff' == current_file_extension.lower():
                    matches.append(current_file)
        return matches

    def get_metadata_from_text(self, path_to_text_file):
        metadata = dict()
        with open(path_to_text_file) as f:
            lines = f.readlines()
        for line in lines:
            if '=' in line:
                current_line = line.split('=')
                if len(current_line) == 2:
                    raw_key = current_line[0].encode('utf-8').strip()
                    if raw_key[0] == '$':
                        raw_key = raw_key.replace('$', '')
                    raw_value = current_line[1].encode('utf-8').strip()
                    metadata[raw_key] = raw_value
            else:
                current_line = line.split(' ')
                if len(current_line) == 2:
                    raw_key = current_line[0].encode('utf-8').strip()
                    if raw_key[0] == '$':
                        raw_key = raw_key.replace('$', '')
                    raw_value = current_line[1].encode('utf-8').strip()
                    metadata[raw_key] = raw_value
        return metadata


if __name__ == "__main__":
    extractor = SemExtractor()
    extractor.start()