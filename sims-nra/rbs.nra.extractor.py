#!/usr/bin/env python

import logging
import os
import re
import subprocess
import tempfile
import sims_nra

import shutil
from pyclowder.extractors import Extractor
import pyclowder.files
import pyclowder.utils


class RbsNraExtractor(Extractor):
    """Count the number of characters, words and lines in a text file."""
    def __init__(self):
        Extractor.__init__(self)

        image_binary = os.getenv('IMAGE_BINARY', '/usr/bin/convert')

        # add any additional arguments to parser
        self.parser.add_argument('--image-binary', nargs='?', dest='image_binary', default=image_binary,
                                 help='Image Binary used to for image thumbnail/preview (default=%s)' % image_binary)

        # parse command line and load default logging configuration
        self.setup()

        # setup logging for the exctractor
        logging.getLogger('pyclowder').setLevel(logging.DEBUG)
        logging.getLogger('__main__').setLevel(logging.DEBUG)

    def process_message(self, connector, host, secret_key, resource, parameters):
        # Process the file and upload the results

        inputfile = resource["local_paths"][0]
        file_id = resource['id']

        (fd, tn_file) = tempfile.mkstemp(suffix=".png")
        (fd2, tn_file_thumbnail) = tempfile.mkstemp(suffix=".png")

        try:
            lines = sims_nra.getLines(inputfile)
            #metadata = sims_nra.getMetadataFromLines(lines)

            data = sims_nra.getActualDataNra(lines)
            simulated_data = sims_nra.getSimulatedFromLines(lines)
            sims_nra.plotDataAndSimulated(data, simulated_data, tn_file)
            print('got plot of data')
            preview_id = pyclowder.files.upload_preview(connector, host, secret_key, file_id, tn_file, None)

            print('generated preview id ', preview_id)

            commands_thumbnail = self.args.image_binary + ' ' + tn_file + ' ' + '-resize 225^' + ' ' + tn_file_thumbnail

            p = re.compile(r'''((?:[^ "']|"[^"]*"|'[^']*')+)''')
            commandline_thumbnail = p.split(commands_thumbnail)[1::2]
            image_thumbnail_to_upload = subprocess.check_output(commandline_thumbnail, stderr=subprocess.STDOUT)

            thumbnail_id = pyclowder.files.upload_thumbnail(connector, host, secret_key, file_id, tn_file_thumbnail)

        except:
            logging.error("could not process file " + inputfile)
            raise

        try:
            os.remove(tn_file)
            os.remove(tn_file_thumbnail)
        except:
            logging.error("Could not remove temporary file")
            raise

if __name__ == "__main__":
    extractor = RbsNraExtractor()
    extractor.start()