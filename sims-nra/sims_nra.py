import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
#test_file = '/Users/helium/Desktop/FW__Use_of_SIMNRA_with_MAESTRO_for_Windows/Au_Cu_Si_01.nra'
#test_plot = '/Users/helium/Desktop/plot1.png'

def getLines(pathToFile):
    f = open(pathToFile, 'r')
    raw_lines = f.readlines()
    lines = []
    f.close()
    for each in raw_lines:
        fixed = each.rstrip('\n')
        fixed = fixed.rstrip('\r')
        fixed = fixed.strip()
        lines.append(fixed)
    return lines

def getSimulatedFromLines(lines):
    start_simulated = 0
    for i in range(0, len(lines)):
        current_line = str(lines[i])
        if ('Simulated' in current_line):
            start_simulated = i
            break
    if (start_simulated != 0):
        values = []
        begin = start_simulated+2
        length = int(str(lines[start_simulated+1]))
        end = begin + length
        for i in range(begin,end):
            current_line = str(lines[i])
            current_line = current_line.split(' ')
            entry = current_line[-1]
            entry = float(entry)
            values.append(entry)
        return values
    return []

def findNextKey(current_index,lines):
    next_key_index = current_index
    for i in range(current_index+1,len(lines)):
        if lines[i][0] == '$':
            next_key_index = i
            return next_key_index
    return next_key_index

def getValueForKey(key_index,lines):
    next_key = findNextKey(key_index,lines)
    value = []
    if (next_key > key_index +2):
        for i in range(key_index+1,next_key):
            value.append(lines[i])
        return value
    else:
        value = lines[key_index+1]
        return value

def getMetadataFromFile(pathToFile):
    lines = getLines(pathToFile)
    md = getMetadataFromLines(lines)
    return md

def getMetadataFromLines(lines):
    metadata = dict()
    data_start = 0
    data_end  = 0
    for i in range(0, len(lines)):
        if '$DATA' in lines[i]:
            data_start = i
        if '$ROI' in lines[i]:
            data_end = i
    counter = 0
    while (counter < len(lines)):
        if (counter >= data_start and counter < data_end):
            counter = data_end
        else:
            current_line = lines[counter]
            if (current_line[0]=='$'):
                value = getValueForKey(counter,lines)
                metadata[current_line]=value
                next_key_index = findNextKey(counter,lines)
                if (next_key_index == counter):
                    counter = len(lines)+1
                else:
                    counter = next_key_index
    return metadata

def getActualDataNra(lines):
    numLines = 0
    start = 0
    end = 0
    for i in range(0, len(lines)):
        if lines[i].endswith('.txt'):
            next = lines[i+1]
            numLines = int(lines[i+1])
            start = i+2
            end = i+2+numLines
            break
    values = []
    for i in range(start, end):
        current_line = lines[i]
        current_line = current_line.split(' ')
        value = current_line[-1]
        value = float(value)
        values.append(value)
    return values

def plotData(the_data,targetFile):
    x_data = np.arange(0, len(the_data))
    y_data = np.array(the_data)
    plt.plot(x_data, y_data, color='red', linestyle='solid')
    plt.xlabel("channel")
    plt.ylabel("counts")
    plt.savefig(targetFile)
    plt.clf()


def plotDataAndSimulated(the_data, the_simulated, targetFile):
    """
    line_up, = plt.plot([1,2,3], label='Line 2')
    line_down, = plt.plot([3,2,1], label='Line 1')
    plt.legend(handles=[line_up, line_down])
    """
    x_data_actual = np.arange(0, len(the_data))
    y_data_actual = np.array(the_data)

    x_data_simulated = np.arange(0, len(the_simulated))
    y_data_simulated = np.array(the_simulated)

    actual, = plt.plot(x_data_actual, y_data_actual, color='red', linestyle='solid', label='Actual')
    simulated, = plt.plot(x_data_simulated, y_data_simulated, color='blue', linestyle='solid', label='Simulated')
    plt.xlabel("channel")
    plt.ylabel("counts")
    plt.legend(loc='upper left')
    plt.savefig(targetFile)
    plt.clf()

def processSimsNraFile(pathToFile, targetFile):
    lines = getLines(pathToFile)
    # metadata = sims_nra.getMetadataFromLines(lines)
    data = getActualDataNra(lines)
    simulated_data = getSimulatedFromLines(lines)
    plotDataAndSimulated(data, simulated_data, targetFile)

def upload_preview_title(connector, host, key, preview_id, title):
    title_md = {"title": title}
    headers = {'Content-Type': 'application/json'}
    url = host + 'api/previews/' + preview_id + '/title?key=' + key
    result = connector.post(url, headers=headers, data=json.dumps(title_md),
                            verify=connector.ssl_verify if connector else True)
    return result


# def main():
#     sample_file = '/Users/helium/Desktop/MATSE FILES/FW__Use_of_SIMNRA_with_MAESTRO_for_Windows/Au_Cu_Si_01.nra'
#     target_file = '/Users/helium/Desktop/plot.png'
#     processSimsNraFile(sample_file,target_file)
#
# if __name__ == "__main__":
#         main()