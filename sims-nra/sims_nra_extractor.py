import logging
import os
import tempfile

import pyclowder.extractors as extractors

import config
import sims_nra


def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, registrationEndpoints,logger

    receiver = 'sims-nra'
    extractorName = config.extractorName
    messageType = config.messageType
    rabbitmqExchange = config.rabbitmqExchange
    rabbitmqURL = config.rabbitmqURL
    playserverKey = config.playserverKey


    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowder.extractors').setLevel(logging.INFO)

    logger = logging.getLogger(receiver)
    logger.setLevel(logging.DEBUG)

    extractors.connect_message_bus(extractorName=extractorName,
                                   messageType=messageType,
                                   processFileFunction=processFile,
                                   rabbitmqExchange=rabbitmqExchange,
                                   rabbitmqURL=rabbitmqURL)


def check_message(parameters):
    print("checking message")
    return True

def processFile(parameters):
    print("processing file")
    input_file = parameters['inputfile']


    if (input_file):
        (fd, tn_file) = tempfile.mkstemp(suffix=".png")
        try:
            lines = sims_nra.getLines(input_file)
            #metadata = sims_nra.getMetadataFromLines(lines)
            data = sims_nra.getActualDataNra(lines)
            simulated_data = sims_nra.getSimulatedFromLines(lines)
            sims_nra.plotDataAndSimulated(data, simulated_data, tn_file)
            print('got plot of data')
            preview_id = extractors.upload_preview(tn_file, parameters, None)
            #upload_preview_title(preview_id, 'counts', parameters)
            # try:
            #     addMetadata(parameters, parameters['host'], parameters['fileid'], json.dumps(metadata))
            # except:
            #     logger.error("could not post metadata")
        except:
            logger.error("could not process file " + input_file)
            raise
        try:
            os.remove(tn_file)
        except:
            logger.error("Could not remove temporary file")
            raise