# name to show in rabbitmq queue list
extractorName="rbs-spe"

# URL to be used for connecting to rabbitmq
#rabbitmqURL = "amqp://guest:guest@localhost/%2f"
rabbitmqURL="amqp://guest:guest@localhost:5672/%2f"
#rabbitmqURL="amqp://guest:guest@192.168.3.153:5672/%2f"

clowderhost="127.0.0.1"
clowderporst="9000"

# name of rabbitmq exchange
rabbitmqExchange="clowder"
playserverKey='phuong-test'

# type of files to process
messageType='*.file.application.simsspe.#'


# trust certificates, set this to false for self signed certificates
sslVerify=False