import numpy as np
import matplotlib
matplotlib.use('Agg')
import json
import matplotlib.pyplot as plt

def getLines(pathToFile):
    f = open(pathToFile, 'r')
    raw_lines = f.readlines()
    lines = []
    f.close()
    for each in raw_lines:
        fixed = each.rstrip('\n')
        fixed = fixed.rstrip('\r')
        fixed = fixed.strip()
        lines.append(fixed)
    return lines

def getDataFromLines(lines):
    start = 0
    end = 0
    for i in range(0, len(lines)):
        if '$DATA' in lines[i]:
            start = (i+2)
        if '$ROI' in lines[i]:
            end = i
    raw_data = lines[start:end]
    data = []
    for i in range(0, len(raw_data)):
        current = int(raw_data[i])
        data.append(current)
    return data

def findNextKey(current_index,lines):
    next_key_index = current_index
    for i in range(current_index+1,len(lines)):
        if lines[i][0] == '$':
            next_key_index = i
            return next_key_index
    return next_key_index

def getValueForKey(key_index,lines):
    next_key = findNextKey(key_index,lines)
    value = []
    if (next_key > key_index +2):
        for i in range(key_index+1,next_key):
            value.append(lines[i])
        return value
    else:
        value = lines[key_index+1]
        return value

def getMetadataFromFile(pathToFile):
    lines = getLines(pathToFile)
    md = getMetadataFromLines(lines)
    return md

def getMetadataFromLines(lines):
    metadata = dict()
    data_start = 0
    data_end  = 0
    for i in range(0, len(lines)):
        if '$DATA' in lines[i]:
            data_start = i
        if '$ROI' in lines[i]:
            data_end = i
    counter = 0
    while (counter < len(lines)):
        if (counter >= data_start and counter < data_end):
            counter = data_end
        else:
            current_line = lines[counter]
            if (current_line[0]=='$'):
                value = getValueForKey(counter,lines)
                current_key = current_line.replace(':','')
                current_key = current_key.replace('$','')
                metadata[current_key]=value
                next_key_index = findNextKey(counter,lines)
                if (next_key_index == counter):
                    counter = len(lines)+1
                else:
                    counter = next_key_index
    return metadata



def plotData(the_data,targetFile):
    x_data = np.arange(0, len(the_data))
    y_data = np.array(the_data)
    plt.plot(x_data, y_data, color='red', linestyle='solid')
    plt.xlabel("channel")
    plt.ylabel("counts")
    plt.savefig(targetFile)
    plt.clf()

def upload_preview_title(connector, host, key, preview_id, title):
    title_md = {"title": title}
    headers = {'Content-Type': 'application/json'}
    url = host + 'api/previews/' + preview_id + '/title?key=' + key
    result = connector.post(url, headers=headers, data=json.dumps(title_md),
                            verify=connector.ssl_verify if connector else True)
    return result