import json
import logging
import os
import tempfile
import sims_spe
import config
import pyclowder.extractors as extractors
import requests
import pika
import time


def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, registrationEndpoints,logger

    receiver = 'rbs-spe'
    extractorName = config.extractorName
    messageType = config.messageType
    rabbitmqExchange = config.rabbitmqExchange
    rabbitmqURL = config.rabbitmqURL
    playserverKey = config.playserverKey


    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowder.extractors').setLevel(logging.INFO)

    logger = logging.getLogger(receiver)
    logger.setLevel(logging.DEBUG)

    extractors.connect_message_bus(
                extractorName=extractorName,
                messageType=messageType,
                processFileFunction=processFile,
                checkMessageFunction=check_message,
                rabbitmqExchange=rabbitmqExchange,
                rabbitmqURL=rabbitmqURL)

def check_message(parameters):
    return True

def processFile(parameters):
    input_file = parameters['inputfile']
    print(input_file)
    if (input_file):
        (fd, tn_file) = tempfile.mkstemp(suffix=".png")
        try:
            lines = sims_spe.getLines(input_file)
            metadata = sims_spe.getMetadataFromLines(lines)
            data = sims_spe.getDataFromLines(lines)
            sims_spe.plotData(data, tn_file)
            preview_id = extractors.upload_preview(tn_file, parameters, None)
            upload_preview_title(preview_id, 'counts', parameters)
            try:
                extractors.upload_file_metadata(metadata,parameters)
                #addMetadata(parameters, parameters['host'], parameters['fileid'], json.dumps(metadata))
            except:
                logger.error("could not post metadata")
        except:
            logger.error("could not process file " + input_file)
            raise
        try:
            os.remove(tn_file)
        except:
            logger.error("Could not remove temporary file")
            raise

def upload_preview_title(previewid, title_name,parameters):
    key = parameters['secretKey']
    title = dict()
    title['title'] = title_name
    host = parameters['host']
    if (not host.endswith("/")):
        host = host + "/"
    url = host + 'api/previews/'+previewid+'/title?key='+key

    headers = {'Content-Type': 'application/json'}

    if url:
        r = requests.post(url, headers=headers, data=json.dumps(title), verify=False);
        r.raise_for_status()
    return r

def upload_preview_title_new(connector, host, key, preview_id, title):
    title_md = {"title": title}
    headers = {'Content-Type': 'application/json'}
    url = host + '/api/previews/' + preview_id + '/title?key=' + key
    result = connector.post(url, headers=headers, data=json.dumps(title_md),
                            verify=connector.ssl_verify if connector else True)
    return result


def addMetadata(parameters,host,file_id,metaDataJson):
    headers = {'Content-Type': 'application/json'}
    url = host +'/api/files/' + file_id + '/metadata?key=' + parameters['secretKey']

    r = requests.post(url, headers=headers, data=metaDataJson, verify=False)
    r.raise_for_status()

if __name__ == "__main__":
    main()
