#!/usr/bin/env python

import logging
import os
import re
import subprocess
import tempfile
import sims_xrna
import shutil
from pyclowder.extractors import Extractor
import pyclowder.files
import pyclowder.utils


class RbsXnraExtractor(Extractor):
    """Count the number of characters, words and lines in a text file."""
    def __init__(self):
        Extractor.__init__(self)

        image_binary = os.getenv('IMAGE_BINARY', '/usr/bin/convert')

        # add any additional arguments to parser
        self.parser.add_argument('--image-binary', nargs='?', dest='image_binary', default=image_binary,
                                 help='Image Binary used to for image thumbnail/preview (default=%s)' % image_binary)

        # parse command line and load default logging configuration
        self.setup()

        # setup logging for the exctractor
        logging.getLogger('pyclowder').setLevel(logging.DEBUG)
        logging.getLogger('__main__').setLevel(logging.DEBUG)

    def process_message(self, connector, host, secret_key, resource, parameters):
        # Process the file and upload the results

        inputfile = resource["local_paths"][0]
        file_id = resource['id']

        (fd, data_and_smoothed) = tempfile.mkstemp(suffix=".png")
        (fd1, simulation) = tempfile.mkstemp(suffix=".png")
        (fd2, elemental) = tempfile.mkstemp(suffix=".png")
        (fd3, isotopes) = tempfile.mkstemp(suffix=".png")
        (fd4, image_thumbnail) = tempfile.mkstemp(suffix=".png")

        try:
            asDict = sims_xrna.getAsDict(inputfile)
            try:
                sims_xrna.plotDataAndSmoothed(asDict, data_and_smoothed)
                print('plotting data')
            except Exception as e:
                print(e)
            try:
                sims_xrna.plotSimulations(asDict, simulation)
                print('plotting simulation')
            except Exception as e:
                print(e)
            try:
                sims_xrna.plotElemental(asDict, elemental)
                print('plotting elemental')
            except Exception as e:
                print(e)
            try:
                sims_xrna.plotIsotope(asDict, isotopes)
                print("plotting isotopes")
            except Exception as e:
                print(e)
            fixed_metadata = sims_xrna.fixDictionary(asDict)
            print('found metadata')

            full_metadata = sims_xrna.removeUnwantedKeys(fixed_metadata)

            metadata = sims_xrna.getMetadataFromDict(full_metadata)

            try:
                preview_id_data_smoothed = pyclowder.files.upload_preview(connector, host, secret_key, file_id, data_and_smoothed, None)
                sims_xrna.upload_preview_title(connector,host, secret_key, preview_id_data_smoothed, "Data and Smoothed")
            except:
                logging.error("could not post preview for data and smoothed")

            try:
                preview_id_simulation = pyclowder.files.upload_preview(connector, host, secret_key, file_id, simulation, None)
                sims_xrna.upload_preview_title(connector,host, secret_key, preview_id_simulation, "Simulation")
            except:
                logging.error("could not post preview for simulation")
            try:
                preview_id_elemental = pyclowder.files.upload_preview(connector, host, secret_key, file_id, elemental, None)
                sims_xrna.upload_preview_title(connector,host, secret_key, preview_id_elemental, "Elemental")
            except:
                logging.error("could not post preview for elemental")

            try:
                preview_id_isotopes = pyclowder.files.upload_preview(connector, host, secret_key, file_id, isotopes, None)
                sims_xrna.upload_preview_title(connector,host, secret_key, preview_id_isotopes, "Isotopes")
            except:
                logging.error("could not post preview for isotopes")

            try:
                file_metadata = self.get_metadata(metadata, 'file', resource['id'], host)
                pyclowder.files.upload_metadata(connector, host, secret_key, file_id, file_metadata)
            except:
                logging.error("could not post metadata")
            #TODO add thumbnail


            try:
                logging.info("generating thumbnail")
                commands_thumbnail = self.args.image_binary + ' ' + data_and_smoothed + ' ' + '-resize 225^' + ' ' + image_thumbnail
                p = re.compile(r'''((?:[^ "']|"[^"]*"|'[^']*')+)''')
                logging.info("thumbnail command")
                logging.info(commands_thumbnail)
                commandline_thumbnail = p.split(commands_thumbnail)[1::2]
                image_thumbnail_to_upload = subprocess.check_output(commandline_thumbnail, stderr=subprocess.STDOUT)

                thumbnail_id = pyclowder.files.upload_thumbnail(connector, host, secret_key, file_id, image_thumbnail)
            except Exception as e:
                logging.error("Did not generate or post thumbnail")
                logging.error(e)


            try:
                os.remove(data_and_smoothed)
                os.remove(simulation)
                os.remove(elemental)
                os.remove(isotopes)
                os.remove(image_thumbnail)
            except:
                logging.error('error removing temporary files')
                logging.error(e)

        except Exception as e:
            logging.error("could not process file ")
            raise


if __name__ == "__main__":
    extractor = RbsXnraExtractor()
    extractor.start()