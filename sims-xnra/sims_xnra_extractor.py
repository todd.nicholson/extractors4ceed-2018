import json
import logging
import os
import tempfile
import sims_xrna
import config
import pyclowder.extractors as extractors
import requests
import pika
import time


def main():
    global extractorName, messageType, rabbitmqExchange, rabbitmqURL, registrationEndpoints,logger

    receiver = 'rbs-xnra'
    extractorName = config.extractorName
    messageType = config.messageType
    rabbitmqExchange = config.rabbitmqExchange
    rabbitmqURL = config.rabbitmqURL
    playserverKey = config.playserverKey


    logging.basicConfig(format='%(levelname)-7s : %(name)s -  %(message)s', level=logging.WARN)
    logging.getLogger('pyclowder.extractors').setLevel(logging.INFO)

    logger = logging.getLogger(receiver)
    logger.setLevel(logging.DEBUG)

    extractors.connect_message_bus(
        extractorName=extractorName,
        messageType=messageType,
        processFileFunction=processFile,
        checkMessageFunction=check_message,
        rabbitmqExchange=rabbitmqExchange,
        rabbitmqURL=rabbitmqURL)


def check_message(parameters):
    print('message received')
    return True


def processFile(parameters):
    print('processing file')
    input_file = parameters['inputfile']
    print(input_file)
    if input_file:
        (fd, data_and_smoothed) = tempfile.mkstemp(suffix=".png")
        (fd1, simulation) = tempfile.mkstemp(suffix=".png")
        (fd2, elemental) = tempfile.mkstemp(suffix=".png")
        (fd3, isotopes) = tempfile.mkstemp(suffix=".png")
        try:
            asDict = sims_xrna.getAsDict(input_file)
            try:
                sims_xrna.plotDataAndSmoothed(asDict, data_and_smoothed)
            except Exception as e:
                print(e)
            try:
                sims_xrna.plotSimulations(asDict, simulation)
            except Exception as e:
                print(e)
            try:
                sims_xrna.plotElemental(asDict, elemental)
            except Exception as e:
                print(e)
            try:
                sims_xrna.plotIsotope(asDict, isotopes)
            except Exception as e:
                print(e)
            fixed_metadata = sims_xrna.fixDictionary(asDict)

            full_metadata = sims_xrna.removeUnwantedKeys(fixed_metadata)

            metadata = sims_xrna.getMetadataFromDict(full_metadata)

            try:
                preview_id_data_smoothed = extractors.upload_preview(data_and_smoothed, parameters, None)
            except:
                logger.error("could not post preview for data and smoothed")

            try:
                upload_preview_title(preview_id_data_smoothed, 'Data and Smoothed', parameters)
            except:
                logger.error('could not provide title for')
            try:
                preview_id_simulation = extractors.upload_preview(simulation, parameters, None)
            except:
                logger.error("could not post preview for simulation")
            try:
                upload_preview_title(preview_id_simulation, 'Simulation', parameters)
            except:
                logger.error('could not provide title for')

            try:
                preview_id_elemental = extractors.upload_preview(elemental, parameters, None)
            except:
                logger.error("could not post preview for elemental")
            try:
                upload_preview_title(preview_id_elemental, 'Elemental', parameters)
            except:
                logger.error('could not provide title for')

            try:
                preview_id_isotopes = extractors.upload_preview(isotopes, parameters, None)
            except:
                logger.error("could not post preview for isotopes")
            try:
                upload_preview_title(preview_id_isotopes, 'Isotopes', parameters)
            except:
                logger.error('could not provide title for')

            try:
                extractors.upload_file_metadata(metadata,parameters)
                #addMetadata(parameters, parameters['host'], parameters['fileid'], json.dumps(metadata))
            except:
                logger.error("could not post metadata")
        except Exception as e:
            logger.error("could not process file ")
            raise
        try:
            os.remove(data_and_smoothed)
        except:
            logger.error("Could not remove temporary file")
        try:
            os.remove(simulation)
        except:
            logger.error("Could not remove temporary file")
        try:
            os.remove(elemental)
        except:
            logger.error("Could not remove temporary file")
        try:
            os.remove(isotopes)
        except:
            logger.error("Could not remove temporary file")

def upload_preview_title(previewid, title_name,parameters):
    key = parameters['secretKey']
    title = dict()
    title['title'] = title_name
    host = parameters['host']
    if (not host.endswith("/")):
        host = host + "/"
    url = host + 'api/previews/'+previewid+'/title?key='+key

    headers = {'Content-Type': 'application/json'}

    if url:
        r = requests.post(url, headers=headers, data=json.dumps(title), verify=False);
        r.raise_for_status()
    return r

def addMetadata(parameters,host,file_id,metaDataJson):
    headers = {'Content-Type': 'application/json'}
    url = host +'/api/files/' + file_id + '/metadata?key=' + parameters['secretKey']

    r = requests.post(url, headers=headers, data=metaDataJson, verify=False)
    r.raise_for_status()

if __name__ == "__main__":
    main()
