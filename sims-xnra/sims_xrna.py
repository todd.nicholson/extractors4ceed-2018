import xmljson
from xmljson import badgerfish as bf
from xml.etree.ElementTree import Element, tostring, fromstring
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import json

# test_file = '/Users/helium/Desktop/FW__Use_of_SIMNRA_with_MAESTRO_for_Windows/xnra/Pt_Mn_Si_Scan01.xnra'

def getLines(pathToFile):
    f = open(pathToFile, 'r')
    content = f.readlines()
    f.close()
    return content

def getContent(pathToFile):
    f = open(pathToFile, 'r')
    content = f.read()
    f.close()
    return content

def getJson(pathToFile):
    content = getContent(pathToFile)
    asJson = json.dumps(bf.data(fromstring(content)))
    asJson= asJson.replace('{http://idf.schemas.itn.pt}','')
    asJson = asJson.replace('{http://www.simnra.com/simnra}','')
    asJson = asJson.replace('$', 'S')
    asJson = asJson.replace('@units', 'units')
    asJson = asJson.replace('@mode', 'mode')
    return asJson

def getAsDict(pathToFile):
    asJson = getJson(pathToFile)
    asDict = json.loads(asJson)
    return asDict

def getDataFromDictionary(asDict):
    keys = list(asDict.keys())
    value = asDict[keys[0]]
    sample = value['sample']

    spectra = sample['spectra']

    spectrum = spectra['spectrum']
    data = spectrum['data']
    return data

def getSmoothedDataFromDictionary(asDict):
    keys = list(asDict.keys())
    value = asDict[keys[0]]
    sample = value['sample']

    spectra = sample['spectra']

    spectrum = spectra['spectrum']
    processeddata = spectrum['processeddata']

    # smoothed data, has graphics and simpledata
    smootheddata = processeddata['smootheddata']
    return smootheddata

def getSimulationsFromData(asDict):
    keys = list(asDict.keys())
    value = asDict[keys[0]]
    sample = value['sample']

    spectra = sample['spectra']

    spectrum = spectra['spectrum']
    process = spectrum['process']

    simulations = process['simulations']

    # a list of dictionaries that have graphics and simpledata
    simulation = simulations['simulation']
    return simulation

def getAllKeyValues(current_dictionary):
    keys = []
    current_keys =list(current_dictionary.keys())
    for each in current_keys:
        keys.append(each)
    for each in keys:
        current_value = current_dictionary[each]
        if (type(current_value) == dict):
            keys_inside = getAllKeyValues(current_value)
            current_keys = current_keys + keys_inside
        else:
            pass
            #print('it is not a dict')
    keys = current_keys
    return keys

def findKeysAndParents(current_dictionary, ancestors):
    keys = list(current_dictionary.keys())
    # for each in keys:
    #     print('key and ancestors', each, ancestors)
    for each in keys:
        current_value = current_dictionary[each]
        if (type(current_value) == dict):
            ancestors.append(each)
            current_keys = findKeysAndParents(current_value,ancestors)
        else:
            print(current_value,'has ancestor', ancestors,each)

def generatePlotFromData(current_data, color, targetFile):
    simpledata = current_data['simpledata']
    graphics = current_data['graphics']
    #TODO handle failure for no legend
    label = 'OTHER'
    try:
        legend = graphics['legend']
        legend_keys = list(legend.keys())
        label = legend[legend_keys[0]]
    except:
        pass
    linecolor = graphics['linecolor']
    linecolor_keys = list(linecolor.keys())
    current_color = linecolor[linecolor_keys[0]]
    #print('label', label, 'color', current_color)

    simpledata = current_data['simpledata']
    x = simpledata['x']
    y = simpledata['y']
    x_keys = list(x.keys())
    y_keys = list(y.keys())

    x_values_string = x[x_keys[0]]
    y_values_string = y[y_keys[0]]

    y_values_string = y_values_string.replace('  ', ' ')

    x_values = x_values_string.split(' ')
    y_values = y_values_string.split(' ')
    # print('number of x and y values', len(x_values), len(y_values))
    x_array = np.arange(0, len(y_values))
    y_array = np.array(y_values)
    plt.plot(x_array, y_array, color=color, linestyle='solid', label=label)
    plt.legend(loc='upper left')
    plt.savefig(targetFile)

def generateColorList(n):
    color = iter(cm.rainbow(np.linspace(0, 1, n)))
    color_list = []
    for i in range(n):
        c = next(color)
        color_list.append(c)
        #ax1.plot(x, y,c=c)
    return color_list

def plotDataAndSmoothed(asDict, targetFile):
    data = getDataFromDictionary(asDict)

    smootheddata = getSmoothedDataFromDictionary(asDict)

    simulations = getSimulationsFromData(asDict)

    all_data = [data] + [smootheddata]
    colors = generateColorList(len(all_data))
    for i in range(0, len(all_data)):
        try:
            generatePlotFromData(all_data[i], colors[i], targetFile)
        except:
            print("failed for", i)
    #plt.legend(loc='upper left')
    # F = plt.gcf()
    # Size = F.get_size_inches()
    #F.set_size_inches(Size[0] * 1.5, Size[1] * 1.5, forward=True)
    plt.sup("Data and smoothed")
    plt.savefig(targetFile)
    plt.clf()

def plotSimulations(asDict, targetFile):
    simulations = getSimulationsFromData(asDict)
    current_simulations = []
    for each in simulations:
        if ('initialtargetparticle' not in list(each.keys())):
            current_simulations.append(each)
    colors = generateColorList(len(current_simulations))
    for i in range(0, len(current_simulations)):
        try:
            generatePlotFromData(current_simulations[i], colors[i], targetFile)
        except:
            print("failed for", i)
    # F = plt.gcf()
    # Size = F.get_size_inches()
    # F.set_size_inches(Size[0] * 1.5, Size[1] * 1.5, forward=True)
    plt.suptitle('Simulations')
    plt.savefig(targetFile)
    plt.clf()


def plotElemental(asDict, targetFile):
    simulations = getSimulationsFromData(asDict)
    current_simulations = []
    elemental = []
    for each in simulations:
        if ('initialtargetparticle' in list(each.keys())):
            current_particle = each['initialtargetparticle']
            current_particle = current_particle['S']
            if (current_particle[:1].isdigit()):
                pass
            else:
                elemental.append(each)
            current_simulations.append(each)
    colors = generateColorList(len(elemental))
    for i in range(0, len(elemental)):
        try:
            generatePlotFromData(elemental[i], colors[i], targetFile)
        except:
            print("failed for", i)
    #plt.legend(loc='upper left')
    F = plt.gcf()
    Size = F.get_size_inches()
    F.set_size_inches(Size[0] * 1.5, Size[1] * 1.5, forward=True)
    plt.suptitle('Elemental')
    plt.savefig(targetFile)
    plt.clf()

def plotIsotope(asDict, targetFile):
    simulations = getSimulationsFromData(asDict)
    current_simulations = []
    isotopes = []
    for each in simulations:
        if ('initialtargetparticle' in list(each.keys())):
            current_particle = each['initialtargetparticle']
            current_particle = current_particle['S']
            #print(current_particle)
            if (current_particle[:1].isdigit()):
                isotopes.append(each)
            current_simulations.append(each)
    colors = generateColorList(len(isotopes))
    for i in range(0, len(isotopes)):
        try:
            generatePlotFromData(isotopes[i], colors[i], targetFile)
        except:
            print("failed for", i)
    #plt.legend(loc='upper left')
    # F = plt.gcf()
    # Size = F.get_size_inches()
    # #F.set_size_inches(Size[0] * 1.2, Size[1] * 1.2, forward=True)
    plt.suptitle('Isotope')
    plt.savefig(targetFile)
    plt.clf()

def plotDataAndSimulations(asDict, targetFile):
    data = getDataFromDictionary(asDict)

    smootheddata = getSmoothedDataFromDictionary(asDict)

    simulations = getSimulationsFromData(asDict)

    all_data_and_simulations = []
    all_data_and_simulations = all_data_and_simulations + [data] + [smootheddata] + simulations
    colors = generateColorList(len(all_data_and_simulations))
    for i in range(0, len(all_data_and_simulations)):
        try:
            generatePlotFromData(all_data_and_simulations[i], colors[i], targetFile)
        except:
            print("failed for", i)
    #plt.legend(loc='upper left')
    # F = plt.gcf()
    # Size = F.get_size_inches()
    #F.set_size_inches(Size[0] * 2, Size[1] * 2, forward=True)
    plt.suptitle('Data and Simulations')
    plt.savefig(targetFile)
    plt.clf()

def delete_keys_from_dict(dict_del, lst_keys):
    for k in lst_keys:
        try:
            del dict_del[k]
        except KeyError:
            pass
    for v in dict_del.values():
        if isinstance(v, dict):
            delete_keys_from_dict(v, lst_keys)
        elif isinstance(v, list):
            if (isinstance(v[0],dict)):
                for each in v:
                    delete_keys_from_dict(each,lst_keys)
    return dict_del

def removeUnwantedKeys(asDict):
    new_dict = delete_keys_from_dict(asDict,['simpledata', 'graphics'])
    return new_dict

def fixDictionary(asDict):
    keys = list(asDict.keys())
    value = asDict[keys[0]]
    keys = list(value.keys())
    sample = value['sample']
    elements_and_molecules = sample['elementsandmolecules']
    structure = sample['structure']
    spectra = sample['spectra']
    spectrum = spectra['spectrum']
    new_dict = dict()
    new_dict['elementsandmolecules'] = elements_and_molecules
    new_dict['structure'] = structure
    new_dict['spectrum'] = spectrum
    return new_dict

def getMetadataFromDict(asDict):
    spectrum = asDict['spectrum']

    metadata = []

    log = spectrum['log']  # YES
    metadata.append(log)

    beam = spectrum['beam']  # YES
    metadata.append(beam)

    geometry = spectrum['geometry']  # YES
    metadata.append(geometry)

    detection = spectrum['detection']  # YES
    metadata.append(detection)

    calibrations = spectrum['calibrations']  # YES
    metadata.append(calibrations)

    reactions = spectrum['reactions']  # YES
    metadata.append(reactions)

    processeddata = spectrum['processeddata']

    data = spectrum['data']
    metadata.append(data)

    smootheddata = processeddata['smootheddata']  # do we need processeddata?
    metadata.append(smootheddata)

    process = spectrum['process']

    physicsdefaults = process['physicsdefaults']  # YES
    metadata.append(physicsdefaults)

    simulations = process['simulations']  # we don't need this

    simulation = simulations['simulation']  # first one is very long
    for each in simulation:
        metadata.append(each)

    scaling = spectrum['scaling']  # YES
    metadata.append(scaling)

    subspectra = spectrum['subspectra']  # YES
    metadata.append(subspectra)

    legendoutsideofchart = spectrum['legendoutsideofchart']  # not needed?

    return metadata

def upload_preview_title(connector, host, key, preview_id, title):
    title_md = {"title": title}
    headers = {'Content-Type': 'application/json'}
    url = host + 'api/previews/' + preview_id + '/title?key=' + key
    result = connector.post(url, headers=headers, data=json.dumps(title_md),
                            verify=connector.ssl_verify if connector else True)
    return result
# asDict = getAsDict(test_file)
#
# plotDataAndSmoothed(asDict,'/Users/helium/Desktop/xnra_smoothed.png')
#
# plotSimulations(asDict,'/Users/helium/Desktop/xnra_simulations.png')
# plotIsotope(asDict, '/Users/helium/Desktop/xnra_iso.png')


#
# fixed = fixDictionary(asDict)
#
# metadata = removeUnwantedKeys(fixed)
#
# metadata = getMetadataFromDict(metadata)



#real data is red
#icons

# asDict2 = getAsDict(test_file)
#
# new_dict = delete_keys_from_dict(asDict2, ['simpledata'])
#
# asJson = json.dumps(new_dict)
# f = open('/Users/helium/Desktop/jsontest.txt','w')
# f.write(asJson)
# f.close()
#
# data = getDataFromDictionary(asDict)
#
# smootheddata = getSmoothedDataFromDictionary(asDict)
#
# simulations = getSimulationsFromData(asDict)




