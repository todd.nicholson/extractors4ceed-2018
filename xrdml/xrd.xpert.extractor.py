#!/usr/bin/env python

import logging
import os
import re
import subprocess
import tempfile
import shutil
import xrd_xpert
from pyclowder.extractors import Extractor
import pyclowder.files
import pyclowder.utils

class XrdXpert(Extractor):
    """Count the number of characters, words and lines in a text file."""
    def __init__(self):
        Extractor.__init__(self)

        image_binary = os.getenv('IMAGE_BINARY', '/usr/bin/convert')
        image_binary = 'convert'

        # add any additional arguments to parser
        self.parser.add_argument('--image-binary', nargs='?', dest='image_binary', default=image_binary,
                                 help='Image Binary used to for image thumbnail/preview (default=%s)' % image_binary)

        # parse command line and load default logging configuration
        self.setup()

        # setup logging for the exctractor
        logging.getLogger('pyclowder').setLevel(logging.DEBUG)
        logging.getLogger('__main__').setLevel(logging.DEBUG)

    def process_message(self, connector, host, secret_key, resource, parameters):
        # Process the file and upload the results

        inputfile = resource["local_paths"][0]
        file_id = resource['id']

        (fd, plot) = tempfile.mkstemp(suffix=".png")
        (fd1, thumbnail_to_upload) = tempfile.mkstemp(suffix=".png")

        try:
            metadata = xrd_xpert.get_metadata(inputfile)

            try:
                file_metadata = self.get_metadata(metadata, 'file', resource['id'], host)
                pyclowder.files.upload_metadata(connector, host, secret_key, file_id, file_metadata)
            except Exception as e:
                logging.error(e)
                logging.error('could not post metadata')

            xrd_xpert.generate_plot(inputfile, plot)



            try:
                pyclowder.files.upload_preview(connector, host, secret_key, file_id, plot, None)
            except Exception as e:
                logging.error("could not post preview")
                logging.error(e)

            try:
                commands_thumbnail = self.args.image_binary + ' ' + plot + ' ' + '-resize 225^' + ' ' + thumbnail_to_upload
                p = re.compile(r'''((?:[^ "']|"[^"]*"|'[^']*')+)''')
                commandline_thumbnail = p.split(commands_thumbnail)[1::2]
                image_thumbnail_to_upload = subprocess.check_output(commandline_thumbnail, stderr=subprocess.STDOUT)
                pyclowder.files.upload_thumbnail(connector, host, secret_key, file_id, thumbnail_to_upload)
            except Exception as e:
                logging.error("could not generate or post thumbnail")
                logging.error(e)
            try:
                os.remove(plot)
                os.remove(thumbnail_to_upload)
            except Exception as e:
                logging.error("could not remove temporary files")
                logging.error(e)

        except Exception as e:
            logging.error(e)
            logging.error("error processing file")

if __name__ == "__main__":
    extractor = XrdXpert()
    extractor.start()